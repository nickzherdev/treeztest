import dotenv from 'dotenv';
import log from '../loaders/logger';

if (process.env.NODE_ENV === 'development') {
  const dotEnvFilePath = `${__dirname}/../../.env`;
  try {
    dotenv.config({ path: dotEnvFilePath });
  } catch (ex) {
    log.warn(
      `Failed to load ${dotEnvFilePath} file. Using default values for Config`,
    );
    log.debug(ex);
  }
}

export default {
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 3000,
  baseUrl: process.env.BASE_URL || '/api',
  dbUrl: process.env.DB_URL || 'mongodb://localhost/TreezTask',
};
