import config from './config/config';
import log from './loaders/logger';
import MongooseLoader from './loaders/mongoose';
import ExpressLoader from './loaders/express';
import routes from './routes';

log.info('Starting the app');

const main = async () => {
  try {
    log.info(`Connecting to DB with url: ${config.dbUrl}`);
    await MongooseLoader(config.dbUrl);
    log.info('Connected...');
    log.info('Initializing Express');
    const expressApp = await ExpressLoader(routes);
    log.info('Done...');
    log.info(`Starting web server with port: ${config.port}`);
    expressApp.listen(config.port, err => {
      if (err) {
        log.error(err);
        process.exit(1);
      }
      log.info(`Done at: http://localhost:${config.port}`);
      log.info(`----------------------------------------`);
      log.info(`Waiting for requests...`);
    });
  } catch (ex) {
    log.error('App failed with Exception: ', ex);
  }
};

main();
