// @flow
import { Schema } from 'mongoose';

const inventoryItemSchema = new Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  availableOnStock: { type: Number, required: true, default: 0 },
  price: { type: Number, required: true, default: 0 },
});

inventoryItemSchema.path('id').index({ unique: true });

// eslint-disable-next-line func-names
inventoryItemSchema.methods.toJSON = function() {
  return {
    id: this.id,
    name: this.name,
    availableOnStock: this.availableOnStock,
    price: this.price,
  };
};

export { inventoryItemSchema };
