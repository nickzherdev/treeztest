import mongoose from 'mongoose';
import { inventoryItemSchema } from './inventoryItemSchema';
import { orderSchema } from './orderSchema';

export const ITEM_MODEL = 'InventoryItem';
export const ORDER_MODEL = 'Order';

export const InventoryItemModel = mongoose.model(
  ITEM_MODEL,
  inventoryItemSchema,
);

export const OrderModel = mongoose.model(ORDER_MODEL, orderSchema);
