// @flow
import { Schema } from 'mongoose';

const orderSchema = new Schema({
  id: { type: String, required: true },
  customerEmail: { type: String, required: true },
  state: { type: String, required: true, default: 'Created' },
  items: [
    {
      id: { type: String, required: true },
      name: { type: String, required: true },
      count: { type: Number, required: true, default: 0 },
      pricePerItem: { type: Number, required: true, default: 0 },
      totalPrice: { type: Number, required: true, default: 0 },
    },
  ],
});

// orderSchema.path('customerEmail').index({ unique: true });

// eslint-disable-next-line func-names
orderSchema.methods.toJSON = function() {
  return {
    id: this.id,
    customerEmail: this.customerEmail,
    state: this.state,
    items: this.items.map(el => ({
      id: el.id,
      name: el.name,
      count: el.count,
      pricePerItem: el.pricePerItem,
      totalPrice: el.totalPrice,
    })),
    totalAmount: this.items
      .map(el => el.totalPrice)
      .reduce((total, next) => total + next),
  };
};

export { orderSchema };
