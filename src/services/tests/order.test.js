import InventoryService from '../inventory';
import OrderService from '../order';
import mongoosLoader from '../../loaders/mongoose';
import config from '../../config/config';

let connection = null;
beforeAll(async () => {
  connection = await mongoosLoader(config.dbUrl);
});

afterAll(() => connection.connection.close());

let recId = null;

describe('Order Service', () => {
  it('should create the instance', () => {
    const service = new OrderService();
    expect(service).toBeTruthy();
  });

  it('should Add the Order', async () => {
    const invService = new InventoryService();
    const orderService = new OrderService();
    const newItem = await invService.addInventoryItem({
      name: 'Test Order Item',
      availableOnStock: 10,
      price: 3,
    });
    const newOrder = await orderService.addOrder({
      customerEmail: 'utittest@test.test',
      state: 'Created',
      items: [
        {
          id: newItem.id,
          count: 3,
        },
      ],
    });
    recId = newOrder.toJSON().id;
    console.log(newOrder.toJSON());
    expect(newOrder).toBeTruthy();
    expect(newOrder.id).toBeTruthy();
  });
});
