import InventoryService from '../inventory';
import mongoosLoader from '../../loaders/mongoose';
import config from '../../config/config';

let connection = null;
beforeAll(async () => {
  connection = await mongoosLoader(config.dbUrl);
});

afterAll(() => connection.connection.close());

let recId = null;

describe('Inventory Service', () => {
  it('should create the instance', () => {
    const service = new InventoryService();
    expect(service).toBeTruthy();
  });

  it('should Add the Item', async () => {
    const service = new InventoryService();
    const newItem = await service.addInventoryItem({
      name: 'Test Item',
      availableOnStock: 10,
      price: 3,
    });
    recId = newItem.id;
    expect(newItem).toBeTruthy();
  });

  it('should Update the Item', async () => {
    const service = new InventoryService();
    const updateRec = {
      name: 'Test Item Updated',
      availableOnStock: 112,
      price: 32,
    };
    const updatedItem = await service.updateInventoryItem(recId, updateRec);
    expect(updatedItem.name).toBe(updateRec.name);
    expect(updatedItem.availableOnStock).toBe(updateRec.availableOnStock);
    expect(updatedItem.price).toBe(updateRec.price);
  });

  it('should Return the list of all Items', async () => {
    const service = new InventoryService();
    const itemsList = await service.getInventoryList();
    expect(itemsList).toBeTruthy();
  });

  it('should Delete the Item', async () => {
    const service = new InventoryService();
    const itemsList = await service.deleteInventoryItem(recId);
    expect(itemsList).toBeTruthy();
  });
});
