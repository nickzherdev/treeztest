import uuid from 'uuid/v4';
// import { InventoryItemModel, OrderModel } from '../model';
import { InventoryItemModel } from '../model';

export const ERR_NOT_FOUND = 'record not found';

export default class InventoryService {
  async addInventoryItem(item) {
    if (!item) {
      throw new Error('item could not be empty');
    }
    const newRecord = new InventoryItemModel({ ...item, id: uuid() });
    await newRecord.save();
    return newRecord.toJSON();
  }

  async updateInventoryItem(id, updatedItem) {
    if (!updatedItem) {
      throw new Error('item could not be empty');
    }
    if (!id) {
      throw new Error('id could not be empty');
    }
    const updatedRec = { ...updatedItem };
    delete updatedRec.id;
    return new Promise((resolve, reject) => {
      InventoryItemModel.findOneAndUpdate({ id }, updatedRec, (err, resp) => {
        if (err) {
          reject(new Error(err));
          return;
        }
        if (!resp) {
          reject(new Error(ERR_NOT_FOUND));
          return;
        }
        resolve({ ...resp.toJSON(), ...updatedRec });
      });
    });
  }

  async deleteInventoryItem(id) {
    if (!id) {
      throw new Error('id could not be empty');
    }
    return new Promise((resolve, reject) => {
      InventoryItemModel.findOneAndDelete({ id }, (err, resp) => {
        if (err) {
          reject(new Error(err));
          return;
        }
        if (!resp) {
          reject(new Error(ERR_NOT_FOUND));
          return;
        }
        resolve(resp);
      });
    });
  }

  async getInventoryList() {
    return new Promise((resolve, reject) => {
      InventoryItemModel.find({}, (err, resp) => {
        if (err) {
          reject(new Error(err));
          return;
        }
        resolve(resp.map(el => el.toJSON()));
      });
    });
  }

  async getInventoryById(id) {
    return new Promise((resolve, reject) => {
      InventoryItemModel.findOne({ id }, async (err, resp) => {
        if (err) {
          reject(new Error(err));
          return;
        }
        if (!resp) {
          reject(new Error(ERR_NOT_FOUND));
          return;
        }
        // const cnt = await calcCount(id);
        resolve({
          ...resp.toJSON(),
          // availableOnStock: cnt,
        });
      });
    });
  }
}

// const calcCount = async itemId => {
//   const orders = await OrderModel.find({ 'items.id': itemId });
//   const itemsArr = orders.map(o => o.items.find(el => el.id === itemId).count);
//   const count = itemsArr.reduce((t, n) => t + n);
//   return count;
// };
