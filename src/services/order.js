import uuid from 'uuid/v4';
import Transaction from 'mongoose-transactions';
import {
  InventoryItemModel,
  OrderModel,
  ITEM_MODEL,
  ORDER_MODEL,
} from '../model';
// import { InventoryService } from './inventory';

export default class OrderService {
  async getOrders() {
    const orders = await OrderModel.find();
    return orders.map(o => o.toJSON());
  }

  async getById(id) {
    const orders = await OrderModel.findOne({ id });
    return orders.toJSON();
  }

  async addOrder(order) {
    validateDTO(order);
    const inventoryItems = await getInventoryItemsByOrder(order);
    const transaction = new Transaction();
    const orderItems = [];
    order.items.forEach(item => {
      const invItem = inventoryItems.find(i => i.id === item.id);
      if (!invItem) {
        transaction.rollback();
        throw new Error(
          `Item with id: ${item.id} does not exists in the Inventory`,
        );
      }
      if (invItem.availableOnStock < item.count) {
        transaction.rollback();
        throw new Error(
          `Order requests more than available on Stock for ${invItem.name}(available: ${invItem.availableOnStock}, requested: ${item.count})`,
        );
      }
      orderItems.push({
        id: item.id,
        name: invItem.name,
        count: item.count,
        pricePerItem: invItem.price,
        totalPrice: invItem.price * item.count,
      });
      // eslint-disable-next-line no-underscore-dangle
      transaction.update(ITEM_MODEL, invItem._id, {
        ...invItem.toJSON(),
        availableOnStock: invItem.availableOnStock - item.count,
      });
    });
    const newOrderId = transaction.insert(ORDER_MODEL, {
      ...order,
      id: uuid(),
      items: orderItems,
    });
    try {
      await transaction.run();
      return OrderModel.findById(newOrderId);
    } catch (ex) {
      transaction.rollback();
      throw ex;
    }
  }

  async updateOrder(id, updatedOrder) {
    validateDTO(updatedOrder);
    const order = await OrderModel.findOne({ id });
    if (!order) {
      throw new Error('Order not found');
    }
    const inventoryItems = await getInventoryItemsByOrder(updatedOrder);
    const transaction = new Transaction();
    const newOrderItems = [];
    updatedOrder.items.forEach(item => {
      const invItem = inventoryItems.find(i => i.id === item.id);
      if (!invItem) {
        transaction.rollback();
        throw new Error(
          `Item with id: ${item.id} does not exists in the Inventory`,
        );
      }
      const prevOrderItem = order.items.find(el => el.id === item.id) || {
        count: 0,
      };
      const availableCnt = invItem.availableOnStock + prevOrderItem.count;
      if (availableCnt < item.count) {
        transaction.rollback();
        throw new Error(
          `Order requests more than available on Stock for ${invItem.name}(available: ${availableCnt}, requested: ${item.count})`,
        );
      }
      newOrderItems.push({
        id: item.id,
        name: invItem.name,
        count: item.count,
        pricePerItem: invItem.price,
        totalPrice: invItem.price * item.count,
      });
      // eslint-disable-next-line no-underscore-dangle
      transaction.update(ITEM_MODEL, invItem._id, {
        ...invItem.toJSON(),
        availableOnStock: availableCnt - item.count,
      });
    });
    // eslint-disable-next-line no-underscore-dangle
    transaction.update(ORDER_MODEL, order._id, {
      ...updatedOrder,
      id,
      items: newOrderItems,
    });
    try {
      await transaction.run();
      // eslint-disable-next-line no-underscore-dangle
      return OrderModel.findById(order._id);
    } catch (ex) {
      transaction.rollback();
      throw ex;
    }
  }

  async deleteOrder(id) {
    if (!id) {
      throw new Error('id could not be empty');
    }
    const order = await OrderModel.findOne({ id });
    if (!order) {
      throw new Error('Order not found');
    }
    const inventoryItems = await getInventoryItemsByOrder(order);
    const transaction = new Transaction();
    order.items.forEach(item => {
      const invItem = inventoryItems.find(i => i.id === item.id);
      if (invItem) {
        // eslint-disable-next-line no-underscore-dangle
        transaction.update(ITEM_MODEL, invItem._id, {
          ...invItem.toJSON(),
          availableOnStock: invItem.availableOnStock + item.count,
        });
      }
    });
    // eslint-disable-next-line no-underscore-dangle
    transaction.remove(ORDER_MODEL, order._id);
    try {
      await transaction.run();
      // eslint-disable-next-line no-underscore-dangle
    } catch (ex) {
      transaction.rollback();
      throw ex;
    }
  }
}

const validateDTO = order => {
  if (!order) {
    throw new Error('Order is required');
  }
  if (!order.items || !order.items.length) {
    throw new Error('Order should contain Items');
  }
};

const getInventoryItemsByOrder = async order => {
  const itemsIdList = order.items.map(el => el.id);
  const inventoryItems = await InventoryItemModel.find({
    id: { $in: itemsIdList },
  });
  if (!inventoryItems && !inventoryItems.length) {
    throw new Error('All order items not found');
  }
  return inventoryItems;
};

// const async findInventoryByIdList = idList => {

// }
