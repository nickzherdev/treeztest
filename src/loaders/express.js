import express from 'express';
import cors from 'cors';

export default async routes => {
  const app = express();
  app.use(express.json());
  app.use(cors());
  app.use(routes);
  return app;
};
