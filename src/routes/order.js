import { Router } from 'express';
import OrderService from '../services/order';

const router = Router();
router.post('/orders', async (req, res) => {
  const service = new OrderService();
  try {
    const newOrder = await service.addOrder(req.body);
    res.json(newOrder);
  } catch (ex) {
    res.status(400).json({ message: ex.message });
  }
});

router.put('/orders/:id', async (req, res) => {
  const service = new OrderService();
  try {
    const newOrder = await service.updateOrder(req.params.id, req.body);
    res.json(newOrder);
  } catch (ex) {
    res.status(400).json({ message: ex.message });
  }
});

router.delete('/orders/:id', async (req, res) => {
  const service = new OrderService();
  try {
    await service.deleteOrder(req.params.id);
    res.json({ message: 'Order successfully deleted' });
  } catch (ex) {
    res.status(400).json({ message: ex.message });
  }
});

router.get('/orders', async (req, res) => {
  const service = new OrderService();
  try {
    const orders = await service.getOrders();
    res.json(orders);
  } catch (ex) {
    res.status(500).json({ message: ex.message });
  }
});

router.get('/orders/:id', async (req, res) => {
  const service = new OrderService();
  try {
    const order = await service.getById(req.params.id);
    res.json(order);
  } catch (ex) {
    res.status(400).json({ message: ex.message });
  }
});

export default router;
