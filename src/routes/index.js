import { Router } from 'express';
import config from '../config/config';
import inventoryRoutes from './inventory';
import orderRoutes from './order';

const router = Router();

router.use(config.baseUrl, inventoryRoutes);
router.use(config.baseUrl, orderRoutes);

export default router;
