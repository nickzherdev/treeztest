import { Router } from 'express';
import InventoryService, { ERR_NOT_FOUND } from '../services/inventory';

const router = Router();
router.get('/inventories', async (req, res) => {
  const service = new InventoryService();
  res.json(await service.getInventoryList());
});

router.get('/inventories/:id', async (req, res) => {
  const service = new InventoryService();
  service
    .getInventoryById(req.params.id)
    .then(rec => res.json(rec))
    .catch(getErrHandler(res));
});

router.post('/inventories', async (req, res) => {
  const service = new InventoryService();
  res.json(await service.addInventoryItem(req.body));
});

router.put('/inventories/:id', async (req, res) => {
  const service = new InventoryService();
  service
    .updateInventoryItem(req.params.id, req.body)
    .then(rec => res.json(rec))
    .catch(getErrHandler(res));
});

router.delete('/inventories/:id', async (req, res) => {
  const service = new InventoryService();
  service
    .deleteInventoryItem(req.params.id)
    .then(rec => res.json(rec))
    .catch(getErrHandler(res));
});

const getErrHandler = res => err =>
  err.message === ERR_NOT_FOUND
    ? res.status(404).json({ message: 'Record not found' })
    : res.status(500).json(err);

export default router;
