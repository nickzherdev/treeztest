module.exports = {
  collectCoverageFrom: ['src/**/*.js', '!src/**/*.test.js', '!src/config/*.js'],
  coverageThreshold: {
    global: {
      statements: 98,
      branches: 91,
      functions: 98,
      lines: 98,
    },
  },
  moduleDirectories: ['node_modules', 'src'],
  setupFilesAfterEnv: ['<rootDir>/internals/testing/test-bundler.js'],
  setupFiles: [],
  testRegex: 'tests/.*\\.test\\.js$',
  snapshotSerializers: [],
  testEnvironment: 'node',
};
